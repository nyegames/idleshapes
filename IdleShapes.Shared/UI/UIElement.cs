﻿using IdleShapes.Shared.PlatformInput;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.TextureAtlases;
using Nez.Textures;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IdleShapes.Shared.UI
{
    /// <summary>An entity that has 4 anchor point entities in an rect around it.
    /// It has a Ninepatch sprite as its main body, the anchors will always be positioned to the edges of the ninepatch.
    /// Has Title text and body text.
    /// TODO Text needs to be resized to stay inside rect bounds given</summary>
    public sealed class UIElement : UIRect, IUniversalInputListener
    {
        /// <summary>How many pixels the title takes up from the top left</summary>
        public Int32 TitleBuffer { get; }

        /// <summary>Pixels required at the bottom of the element</summary>
        public Int32 LowerBuffer { get; }

        /// <summary>Smallest size this element is allowed to be</summary>
        public Int32 MinHeight { get; }

        private Text _TitleTextComponent;
        private NineSliceSprite _BackgroundComponent;

        private String _Title;
        public String Title
        {
            get => _Title;
            set
            {
                _Title = value;
                if (_TitleTextComponent != null) _TitleTextComponent.text = value;
            }
        }

        /// <summary></summary>
        private List<UIBody> _Bodies = new List<UIBody>();

        public UIBody[] Bodies => _Bodies.ToArray();

        public Boolean Open { get; private set; }

        public UIElement(String name, Int32 titleBuffer = 30, Int32 lowerBuffer = 12, Int32 minHeight = 50) : base(name)
        {
            LowerBuffer = lowerBuffer;
            MinHeight = minHeight;
            TitleBuffer = titleBuffer;
        }

        protected override void SetSize(Vector2 size)
        {
            if (size.Y < MinHeight) size = new Vector2(size.X, MinHeight);

            base.SetSize(size);

            if (_BackgroundComponent != null)
            {
                _BackgroundComponent.width = size.X;
                _BackgroundComponent.height = size.Y;
            }
        }

        public void AddBody(UIBody body)
        {
            //If first body
            if (!_Bodies.Any())
            {
                //Parent it to the top left of the element
                body.parent = TopLeft.transform;
                body.transform.localPosition = new Vector2(0, TitleBuffer);
            }
            else
            {
                body.parent = _Bodies.Last().BottomLeft.transform;
            }
            _Bodies.Add(body);

            body.enabled = this.enabled && Open;
        }

        #region Overrides of Entity

        public override void onAddedToScene()
        {
            base.onAddedToScene();

            TextureAtlas uiNinePatch = scene.content.Load<TextureAtlas>("UI/MetalPanelBlue");
            NinePatchSubtexture subTexture = uiNinePatch.getSubtexture("metalPanel_blue") as NinePatchSubtexture;
            _BackgroundComponent = new NineSliceSprite(subTexture)
            {
                width = Screen.width,
                height = TitleBuffer + LowerBuffer,
                localOffset = new Vector2(0, 0)
            };
            addComponent(_BackgroundComponent);

            addComponent(_TitleTextComponent = new Text(new NezSpriteFont(scene.content.Load<SpriteFont>("UI/UIFont")),
                _Title ?? "", new Vector2(7, 0), Color.White));

            SetSize(_Size != Vector2.Zero ? _Size : new Vector2(_BackgroundComponent.width, _BackgroundComponent.height));

            foreach (UIBody uiBody in Bodies)
            {
                this.scene.addEntity(uiBody);
            }

            UniversalInput.Register(this);
        }

        #endregion

        #region Implementation of IUniversalInputListener

        public void OnPressed(int id, Vector2 screenPosition)
        {
            Rectangle elementRect = new Rectangle(transform.position.ToPoint(), Size.ToPoint());

            if (!elementRect.Contains(screenPosition)) return;

            Single duration = 0.13f * (_Bodies.Count + 1);
            if (Open) Exit(duration);
            else Enter(duration);

            Open = !Open;
        }

        public void OnReleased(int id, Vector2 screenPosition)
        {

        }

        public void OnHeld(int id, Vector2 screenPosition)
        {

        }

        #endregion
    }
}
