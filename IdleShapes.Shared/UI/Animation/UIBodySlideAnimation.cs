﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Tweens;
using System;

namespace IdleShapes.Shared.UI.Animation
{
    public class UIBodySlideAnimation : UIRectAnimation
    {
        private Vector2 _LocalPosition;

        private Int32 _Direction;

        public UIBodySlideAnimation(Int32 direction)
        {
            _Direction = direction;
        }

        #region Overrides of UIBodySlideAnimation

        public override void Enter(UIRect uiRect, Single duration, Single delay = 0)
        {
            uiRect.InputEnabled = false;
            _LocalPosition = uiRect.transform.localPosition;

            uiRect.transform.localPosition -= new Vector2(uiRect.Size.X, 0);
            ITween<Vector2> bt = uiRect.transform.tween("localPosition", uiRect.transform.localPosition + new Vector2(uiRect.Size.X, 0), duration);
            bt.setEaseType(EaseType.BackOut);

            bt.setDelay(delay);
            bt.start();

            Core.schedule(delay, s =>
            {
                uiRect.InputEnabled = true;
                uiRect.enabled = true;
            });
        }

        public override void Exit(UIRect uiRect, Single duration, Single delay = 0)
        {
            uiRect.InputEnabled = false;

            ITween<Vector2> bt = uiRect.transform.tween("localPosition", uiRect.transform.localPosition - new Vector2(uiRect.Size.X, 0), duration);
            bt.setEaseType(EaseType.BackIn);
            bt.setDelay(delay);
            bt.start();

            Core.schedule(delay + duration, s =>
            {
                uiRect.InputEnabled = false;
                uiRect.enabled = false;
            });
        }

        public override void Reset(UIRect uiRect)
        {
            uiRect.InputEnabled = true;
            uiRect.transform.localPosition = _LocalPosition;
        }

        #endregion
    }
}
