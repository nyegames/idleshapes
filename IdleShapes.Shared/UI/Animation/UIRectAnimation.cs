﻿using System;

namespace IdleShapes.Shared.UI.Animation
{
    public abstract class UIRectAnimation
    {
        public virtual Object Data { get; set; }

        public abstract void Enter(UIRect uiRect, Single duration, Single delay = 0);

        public abstract void Exit(UIRect uiRect, Single duration, Single delay = 0);

        public abstract void Reset(UIRect uiRect);
    }
}
