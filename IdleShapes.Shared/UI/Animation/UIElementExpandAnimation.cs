﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Tweens;
using System;
using System.Linq;

namespace IdleShapes.Shared.UI.Animation
{
    public class UIElementExpandAnimation : UIRectAnimation
    {
        private Vector2 _StartingSize;

        private ITween<Vector2> _AnimatingTween;

        public override void Enter(UIRect uiRect, Single duration, Single delay = 0)
        {
            uiRect.InputEnabled = false;

            UIElement element = (UIElement)uiRect;
            _StartingSize = element.Size;
            UIBody[] bodies = element.Bodies;

            int buffer = element.TitleBuffer + element.LowerBuffer;
            Vector2 expandSize = new Vector2(Screen.width, Math.Max(buffer, bodies.Sum(s => s.Rect.Height) + buffer));
            _AnimatingTween = uiRect.tween("Size", expandSize, duration);

            _AnimatingTween.setCompletionHandler(s =>
            {
                uiRect.InputEnabled = true;

                foreach (UIBody b in bodies)
                {
                    b.InputEnabled = b.enabled = true;
                }
            });
            _AnimatingTween.setEaseType(EaseType.BackOut);
            _AnimatingTween.start();

            for (int i = 0; i < bodies.Length; i++)
            {
                UIBody b = bodies[i];
                float durationSplit = duration / bodies.Length;
                b.Enter(durationSplit, (Single)i * durationSplit);
            }
        }

        public override void Exit(UIRect uiRect, Single duration, Single delay = 0)
        {
            uiRect.InputEnabled = false;
            UIElement element = (UIElement)uiRect;
            UIBody[] bodies = element.Bodies;

            int buffer = element.TitleBuffer + element.LowerBuffer;
            Vector2 expandSize = new Vector2(Screen.width, buffer);
            _AnimatingTween = uiRect.tween("Size", expandSize, duration);

            _AnimatingTween.setCompletionHandler(s =>
            {
                uiRect.InputEnabled = true;

                foreach (UIBody b in bodies)
                {
                    b.Reset();
                    b.enabled = false;
                }
            });
            _AnimatingTween.setEaseType(EaseType.BackIn);
            _AnimatingTween.start();

            //Reverse to do it backwards
            for (int i = bodies.Length - 1; i >= 0; i--)
            {
                int j = (bodies.Length - 1) - i;
                float durationSplit = duration / bodies.Length;
                float bdelay = (Single)j * durationSplit;

                UIBody b = bodies[i];
                b.Exit(durationSplit, bdelay);
            }
        }

        public override void Reset(UIRect uiRect)
        {
            _AnimatingTween.stop(true);

            uiRect.InputEnabled = false;

            ((UIElement)uiRect).Size = _StartingSize;

            foreach (UIBody uiBody in ((UIElement)uiRect).Bodies) uiBody.Reset();
        }
    }
}
