﻿using IdleShapes.Shared.Data;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using System;

namespace IdleShapes.Shared.UI
{
    public class UIBody : UIRect, IUpdatable
    {
        private ShapeFactory _Factory;

        private Text _BodyTextComponent;

        private String _Text;
        public String Text
        {
            get => _Text;
            set
            {
                _Text = value;
                if (_BodyTextComponent != null)
                {
                    _BodyTextComponent.text = value;
                    SetSize(_BodyTextComponent.bounds.size);
                }
            }
        }

        public UIBody(String name) : base(name)
        {

        }

        public void SetFactory(ShapeFactory factory)
        {
            _Factory = factory;
        }

        public override void onAddedToScene()
        {
            base.onAddedToScene();

            addComponent(_BodyTextComponent = new Text(new NezSpriteFont(scene.content.Load<SpriteFont>("UI/UIFont")),
                _Text ?? "", new Vector2(7, 0), Color.Black));

            SetSize(_BodyTextComponent.bounds.size);
        }

        #region Overrides of Entity

        public override void update()
        {
            base.update();
            if (_Factory != null)
            {
                Text = $"{_Factory.ShapeProduct.Rarity} : {_Factory.ProductsCreated}";
            }
        }

        #endregion


    }
}
