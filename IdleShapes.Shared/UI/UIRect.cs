﻿using IdleShapes.Shared.UI.Animation;
using Microsoft.Xna.Framework;
using Nez;
using System;

namespace IdleShapes.Shared.UI
{
    public class UIRect : Entity
    {
        private UIRectAnimation _Animation;

        public Entity TopLeft => this;
        public Entity TopRight, BottomLeft, BottomRight, Center;

        protected Vector2 _Size;
        public Vector2 Size
        {
            get => _Size;
            set => SetSize(value);
        }

        public Rectangle Rect => new Rectangle(transform.position.ToPoint(), _Size.ToPoint());

        public Boolean InputEnabled { get; set; } = false;
        
        public UIRect(String name) : base(name)
        {
            TopRight = new Entity($"{this.name}-TR");
            TopRight.transform.parent = transform;

            Center = new Entity($"{this.name}-TR");
            Center.transform.parent = transform;

            BottomLeft = new Entity($"{this.name}-BL");
            BottomLeft.transform.parent = transform;

            BottomRight = new Entity($"{this.name}-BR");
            BottomRight.transform.parent = transform;

            _Size = new Vector2(Screen.width, 50);

            transform.localPosition += new Vector2(1, 1);
            transform.localPosition -= new Vector2(1, 1);
        }

        protected virtual void SetSize(Vector2 size)
        {
            _Size = size;

            TopRight.transform.localPosition = new Vector2(_Size.X, 0f);

            Center.transform.localPosition = new Vector2(_Size.X / 2f, _Size.Y / 2f);

            BottomLeft.transform.localPosition = new Vector2(0, _Size.Y);
            BottomRight.transform.localPosition = new Vector2(_Size.X, _Size.Y);
        }

        public void SetAnimation(UIRectAnimation animation) => _Animation = animation;

        public virtual void Enter(Single duration, Single delay = 0)
        {
            _Animation?.Enter(this, duration, delay);
        }

        public virtual void Exit(Single duration, Single delay = 0)
        {
            _Animation?.Exit(this, duration, delay);
        }

        public virtual void Reset()
        {
            _Animation?.Reset(this);
        }
    }
}
