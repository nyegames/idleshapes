﻿using Microsoft.Xna.Framework;
using System;
using Boolean = System.Boolean;

namespace IdleShapes.Shared.PlatformInput
{
    public interface IUniversalInputListener
    {
        Boolean InputEnabled { get; set; }

        void OnPressed(Int32 id, Vector2 screenPosition);

        void OnReleased(Int32 id, Vector2 screenPosition);

        void OnHeld(Int32 id, Vector2 screenPosition);
    }
}
