﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IdleShapes.Shared.PlatformInput
{
    public static class UniversalInput
    {
        private static readonly List<IUniversalInputListener> _InputListeners = new List<IUniversalInputListener>();

        internal class TouchData
        {
            public Int32 ID { get; set; }
            public Vector2 Position { get; set; }

            public enum TouchState
            {
                RELEASED,
                PRESSED,
                HELD
            }

            public TouchState State { get; set; }
        }

        private static readonly Dictionary<Int32, TouchData> _TouchData = new Dictionary<Int32, TouchData>();

        public static Action<Int32, Vector2> OnPressed;
        public static Action<Int32, Vector2> OnHeld;
        public static Action<Int32, Vector2> OnReleased;

        public static void init()
        {
            Input.touch.enableTouchSupport();

            _TouchData.Add(0, new TouchData() { ID = 0, State = TouchData.TouchState.RELEASED });
            _TouchData.Add(1, new TouchData() { ID = 1, State = TouchData.TouchState.RELEASED });


        }

        public static void update(TimeSpan delta)
        {
            #region Mouse

            _TouchData[0].Position = Input.scaledMousePosition;
            _TouchData[1].Position = Input.scaledMousePosition;


            if (Input.leftMouseButtonPressed)
            {
                _TouchData[0].State = TouchData.TouchState.PRESSED;
                for (int i = _InputListeners.Count - 1; i >= 0; i--)
                {
                    if (!_InputListeners[i].InputEnabled) continue;
                    _InputListeners[i].OnPressed(0, _TouchData[0].Position);
                }
                OnPressed?.Invoke(0, _TouchData[0].Position);
            }
            else if (Input.leftMouseButtonDown)
            {
                _TouchData[0].State = TouchData.TouchState.HELD;
                for (int i = _InputListeners.Count - 1; i >= 0; i--)
                {
                    if (!_InputListeners[i].InputEnabled) continue;
                    _InputListeners[i].OnHeld(0, _TouchData[0].Position);
                }
                OnHeld?.Invoke(0, _TouchData[0].Position);
            }
            else if (Input.leftMouseButtonReleased)
            {
                _TouchData[0].State = TouchData.TouchState.RELEASED;
                for (int i = _InputListeners.Count - 1; i >= 0; i--)
                {
                    if (!_InputListeners[i].InputEnabled) continue;
                    _InputListeners[i].OnReleased(0, _TouchData[0].Position);
                }
                OnReleased?.Invoke(0, _TouchData[0].Position);
            }

            if (Input.rightMouseButtonPressed)
            {
                _TouchData[1].State = TouchData.TouchState.PRESSED;
                for (int i = _InputListeners.Count - 1; i >= 0; i--)
                {
                    if (!_InputListeners[i].InputEnabled) continue;
                    _InputListeners[i].OnPressed(1, _TouchData[1].Position);
                }
                OnPressed?.Invoke(1, _TouchData[1].Position);
            }
            else if (Input.rightMouseButtonDown)
            {
                _TouchData[1].State = TouchData.TouchState.HELD;
                for (int i = _InputListeners.Count - 1; i >= 0; i--)
                {
                    if (!_InputListeners[i].InputEnabled) continue;
                    _InputListeners[i].OnHeld(1, _TouchData[1].Position);
                }
                OnHeld?.Invoke(1, _TouchData[1].Position);
            }
            else if (Input.rightMouseButtonReleased)
            {
                _TouchData[1].State = TouchData.TouchState.RELEASED;
                for (int i = _InputListeners.Count - 1; i >= 0; i--)
                {
                    if (!_InputListeners[i].InputEnabled) continue;
                    _InputListeners[i].OnReleased(1, _TouchData[1].Position);
                }
                OnReleased?.Invoke(1, _TouchData[1].Position);
            }

            #endregion


            if (Input.touch.isConnected)
            {
                foreach (TouchLocation touch in Input.touch.currentTouches)
                {
                    if (!_TouchData.ContainsKey(touch.Id)) _TouchData.Add(touch.Id, new TouchData());

                    TouchData td = _TouchData[touch.Id];
                    td.Position = touch.scaledPosition();

                    switch (touch.State)
                    {
                        case TouchLocationState.Moved:
                            td.State = TouchData.TouchState.HELD;
                            for (int i = _InputListeners.Count - 1; i >= 0; i--)
                            {
                                if (!_InputListeners[i].InputEnabled) continue;
                                _InputListeners[i].OnHeld(td.ID, td.Position);
                            }
                            OnHeld?.Invoke(td.ID, td.Position);
                            break;

                        case TouchLocationState.Pressed:
                            td.State = TouchData.TouchState.PRESSED;
                            for (int i = _InputListeners.Count - 1; i >= 0; i--)
                            {
                                if (!_InputListeners[i].InputEnabled) continue;
                                _InputListeners[i].OnPressed(td.ID, td.Position);
                            }
                            OnPressed?.Invoke(td.ID, td.Position);
                            break;
                        case TouchLocationState.Released:
                            td.State = TouchData.TouchState.RELEASED;
                            for (int i = _InputListeners.Count - 1; i >= 0; i--)
                            {
                                if (!_InputListeners[i].InputEnabled) continue;
                                _InputListeners[i].OnReleased(td.ID, td.Position);
                            }
                            OnReleased?.Invoke(td.ID, td.Position);
                            break;

                        case TouchLocationState.Invalid:
                            break;
                    }
                }
            }
        }

        public static Boolean GetScreenPress(Int32 touchIndex, out Vector2 screenPosition)
        {
            TouchData mousePress = _TouchData.FirstOrDefault(s => (s.Key == touchIndex || touchIndex == -1) && s.Value.State == TouchData.TouchState.PRESSED).Value;
            screenPosition = mousePress?.Position ?? Vector2.Zero;
            return mousePress != null;
        }

        public static void Register(IUniversalInputListener listener)
        {
            if (_InputListeners.Contains(listener)) return;
            _InputListeners.Add(listener);
        }

        public static void Unregister(IUniversalInputListener listener)
        {
            if (!_InputListeners.Contains(listener)) return;
            _InputListeners.Remove(listener);
        }

    }
}
