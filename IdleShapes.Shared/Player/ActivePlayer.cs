﻿using IdleShapes.Shared.Data.Player;

namespace IdleShapes.Shared.Player
{
    public static class ActivePlayer
    {
        public static PlayerID ID { get; set; }
    }
}
