﻿using System;

namespace IdleShapes.Shared.Data
{
    public interface IGameWorldListener
    {
        /// <summary>Called when a <see cref="ShapeFactory"/> produces one of its products</summary>
        /// <param name="factory">The factory that has just produces a product</param>
        /// <param name="productsCreated"></param>
        void OnFactoryProductProduced(ShapeFactory factory, Int64 productsCreated);
    }
}
