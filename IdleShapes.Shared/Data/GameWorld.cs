﻿using IdleShapes.Shared.Data.Player;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IdleShapes.Shared.Data
{
    /// <summary>Stores the overall conditions of the game World.
    /// Tracks time progression the player experience based on the current time you set it, and the previous time stored in data</summary>
    public static class GameWorld
    {
        private static readonly List<IGameWorldListener> _Listeners = new List<IGameWorldListener>();

        private static TimeSpan _AutoSaveInterval = TimeSpan.FromMinutes(0.5);

        private static TimeSpan _TimeBetweenSaves;

        [Serializable]
        private struct GameData
        {
            public DateTime CurrentDate { get; set; }
        }

        /// <summary>Current World instance of the <see cref="GameData"/> only 1 required for all players</summary>
        private static GameData _GameData;

        /// <summary>Folder to save all players into</summary>
        private static String _SaveFolder;

        /// <summary>All actively known player inventories</summary>
        private static readonly List<PlayerInventory> PlayerInventories = new List<PlayerInventory>();

        /// <summary>The current active player</summary>
        private static PlayerID _ActivePlayer;

        private static PlayerInventory _ActivePlayerInventory;

        #region Initialise

        /// <summary>Initial call to GameWorld, will restore the current game world to any previously stored data</summary>
        public static void Init()
        {
            _SaveFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ShapeFactory", "Saves");

            LoadGameSeed(_SaveFolder);

            foreach (string guidFolder in Directory.GetDirectories(_SaveFolder))
            {
                //If the player load in this folder has failed.. uh oh..
                if (!LoadPlayers(guidFolder))
                {
                    Console.WriteLine($"Missing player save file, deleting save folder");
                    Directory.Delete(guidFolder);
                    continue;
                }
            }
        }

        /// <summary>Load the game data class, this will manage the progress through "time" during different persistant play sessions</summary>
        /// <param name="saveFolder"></param>
        /// <returns></returns>
        private static void LoadGameSeed(string saveFolder)
        {
            if (!Directory.Exists(saveFolder)) Directory.CreateDirectory(saveFolder);
            string[] gameDataFiles = Directory.GetFiles(saveFolder, $"{nameof(GameData)}*.json");

            if (!gameDataFiles.Any())
            {
                _GameData = new GameData
                {
                    CurrentDate = DateTime.UtcNow
                };
                Save();
            }
            else
            {
                try
                {
                    _GameData = ReadFromJsonFile<GameData>(gameDataFiles.First());
                }
                catch (Exception e)
                {
                    File.Delete(gameDataFiles.First());
                    LoadGameSeed(saveFolder);
                }
            }
        }

        /// <summary>Load all of the available players</summary>
        /// <param name="guidFolder"></param>
        /// <returns></returns>
        private static Boolean LoadPlayers(string guidFolder)
        {
            string playerGuid = guidFolder.Split(Path.DirectorySeparatorChar).Last();
            string[] playerInventoryFiles = Directory.GetFiles(guidFolder, $"{nameof(PlayerInventory)}*.json");
            if (!playerInventoryFiles.Any())
            {
                return false;
            }

            PlayerInventory inventory = ReadFromJsonFile<PlayerInventory>(playerInventoryFiles.First());

            if (!playerGuid.Equals(inventory?.Owner?.ID.ToString()))
            {
                Console.WriteLine($"Modified player inventory detected, Naughty {inventory.Owner.Name}");

                PlayerID newOwner = new PlayerID()
                {
                    Name = inventory.Owner.Name,
                    ID = Guid.Parse(playerGuid)
                };
                inventory.Owner = newOwner;
            }

            PlayerInventories.Add(inventory);

            return true;
        }

        /// <summary>Updates the players inventory based on how much time has passed, given the current <see cref="GameData"/></summary>
        /// <param name="playerID"></param>
        public static void Load(PlayerID playerID)
        {
            _ActivePlayer = playerID;
            _ActivePlayerInventory = GetInventory(_ActivePlayer);

            TimeSpan timePassed = DateTime.UtcNow - _GameData.CurrentDate;

            //If negative time has passed, some cheating has occured, do nothing
            if (timePassed.Ticks < 0) return;

            _GameData.CurrentDate = DateTime.UtcNow;
            Update(timePassed);
        }

        #endregion

        #region Player

        /// <summary>Collects all of the available players that are stored in the GameWorld</summary>
        /// <returns></returns>
        public static PlayerID[] GetPlayers()
        {
            return PlayerInventories.Select(s => s.Owner).ToArray();
        }

        /// <summary>Creates a new player with a given human name</summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static PlayerID CreatePlayer(String name)
        {
            PlayerID player = new PlayerID
            {
                Name = name,
                ID = Guid.NewGuid()
            };
            PlayerInventories.Add(new PlayerInventory { Owner = player });
            return player;
        }

        public static PlayerInventory GetInventory(PlayerID playerID)
        {
            return PlayerInventories.First(s => playerID.Equals(s.Owner));
        }

        /// <summary>Registers a new <see cref="ShapeFactory"/> to the player with the given <see cref="PlayerID"/></summary>
        /// <param name="playerID"></param>
        /// <param name="product"></param>
        /// <param name="productionRate"></param>
        public static void AddFactory(PlayerID playerID, ShapeData product, TimeSpan productionRate)
        {
            PlayerInventory inventory = GetInventory(playerID);
            inventory.AddFactory(product, productionRate);
            Save();
        }

        #endregion

        public static void Update(TimeSpan delta)
        {
            _TimeBetweenSaves += delta;
            _GameData.CurrentDate += delta;

            if (_ActivePlayer == null) return;

            for (int i = _ActivePlayerInventory.OwnedFactories.Count - 1; i >= 0; i--)
            {
                ShapeFactory ownedFactory = _ActivePlayerInventory.OwnedFactories[i];
                long productCount = ownedFactory.ProductsCreated;

                ownedFactory.Update(delta);

                if (ownedFactory.ProductsCreated > productCount)
                {
                    long created = ownedFactory.ProductsCreated - productCount;
                    for (int index = _Listeners.Count - 1; index >= 0; index--)
                    {
                        IGameWorldListener gameWorldListener = _Listeners[index];
                        gameWorldListener.OnFactoryProductProduced(ownedFactory, created);
                    }
                }
            }

            if (_TimeBetweenSaves > _AutoSaveInterval)
            {
                Save();
            }
        }

        public static void Save(Boolean onlyActivePlayer = true)
        {
            if (String.IsNullOrEmpty(_SaveFolder)) return;

            _TimeBetweenSaves = TimeSpan.Zero;

            string gameDataFilePath = Path.Combine(_SaveFolder, $"{nameof(GameData)}.json");
            WriteToJsonFile(gameDataFilePath, _GameData);

            foreach (PlayerInventory playerInventory in PlayerInventories)
            {
                if (onlyActivePlayer && !playerInventory.Owner.Equals(_ActivePlayer)) continue;

                string savePath = Path.Combine(_SaveFolder, playerInventory.Owner.ID.ToString());
                Directory.CreateDirectory(savePath);
                savePath = Path.Combine(savePath, $"{nameof(PlayerInventory)}.json");
                WriteToJsonFile(savePath, playerInventory, false);
            }
        }

        #region Listeners

        public static void Register(IGameWorldListener listener)
        {
            if (_Listeners.Contains(listener)) return;
            _Listeners.Add(listener);
        }

        public static void UnRegister(IGameWorldListener listener)
        {
            if (!_Listeners.Contains(listener)) return;
            _Listeners.Remove(listener);
        }

        #endregion

        #region Save

        /// <summary>
        /// Writes the given object instance to a Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [JsonIgnore] attribute.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        private static void WriteToJsonFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                string contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite, Formatting.Indented);
                writer = new StreamWriter(filePath, append);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                writer?.Close();
            }
        }

        /// <summary>
        /// Reads an object instance from an Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the Json file.</returns>
        private static T ReadFromJsonFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(filePath);
                string fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(fileContents);
            }
            finally
            {
                reader?.Close();

            }
        }

        #endregion
    }
}
