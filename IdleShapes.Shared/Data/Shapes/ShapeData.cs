﻿using System;

namespace IdleShapes.Shared.Data
{
    [Serializable]
    public sealed class ShapeData
    {
        [Serializable]
        public enum ShapeStyle
        {
            CIRCLE,
            TRIANGLE,
            RECTANGLE,
            SQUARE,
            RHOMBUS,
            TRAPEZOID,
            PENTAGON,
            HEXAGON,
            HEPTAGON,
            OCTAGON,
            NANAGON,
            DECOGON,
        }

        [Serializable]
        public enum ShapeRarity
        {
            WHITE = 1,
            GREEN = 2,
            BLUE = 3,
            PURPLE = 4,
            ORANGE = 5,
            YELLOW = 6
        }

        public ShapeStyle Style { get; set; }

        public ShapeRarity Rarity { get; set; }
    }
}
