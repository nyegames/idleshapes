﻿using System;
using System.Collections.Generic;

namespace IdleShapes.Shared.Data
{
    [Serializable]
    public sealed class ShapeFactory
    {
        /// <summary>What the factory produces</summary>
        public ShapeData ShapeProduct { get; set; }

        /// <summary>How long it takes to produce 1 <see cref="ShapeProduct"/></summary>
        public TimeSpan ProductionRate { get; set; }

        /// <summary>How long the factory has currently been working on creating a product</summary>
        public TimeSpan CurrentProductionTime { get; set; }

        /// <summary>All products that have been created by this factory</summary>
        public Int64 ProductsCreated { get; set; }

        /// <summary>All products that have been taken out of the factory</summary>
        public Int64 ProductsTaken { get; set; }

        private Int64 _ProductsSinceSave;
        private static Dictionary<ShapeData.ShapeRarity, Int64> _ProductsRequiredPerRarity = new Dictionary<ShapeData.ShapeRarity, long>()
        {
            { ShapeData.ShapeRarity.WHITE, 5000},
            { ShapeData.ShapeRarity.GREEN, 2000},
            { ShapeData.ShapeRarity.BLUE, 500},
            { ShapeData.ShapeRarity.PURPLE, 100},
            { ShapeData.ShapeRarity.ORANGE, 50},
            { ShapeData.ShapeRarity.YELLOW, 10},
        };

        public void Update(TimeSpan delta)
        {
            CurrentProductionTime += delta;

            Int64 products = Math.DivRem(CurrentProductionTime.Ticks, ProductionRate.Ticks, out Int64 remainder);
            CurrentProductionTime = TimeSpan.FromTicks(remainder);

            if (products > 0)
            {
                ProductsCreated += products;
                _ProductsSinceSave += products;

                if (_ProductsSinceSave > _ProductsRequiredPerRarity[ShapeProduct.Rarity])
                {
                    _ProductsSinceSave = 0;
                    GameWorld.Save();
                }
            }
        }
    }
}
