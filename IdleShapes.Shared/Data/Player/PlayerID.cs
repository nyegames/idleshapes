﻿using System;

namespace IdleShapes.Shared.Data.Player
{
    [Serializable]
    public sealed class PlayerID
    {
        public Guid ID { get; set; }

        public String Name { get; set; }
    }
}
