﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IdleShapes.Shared.Data.Player
{
    [Serializable]
    public sealed class PlayerInventory
    {
        public PlayerID Owner { get; set; }

        public List<ShapeFactory> OwnedFactories { get; private set; } = new List<ShapeFactory>();

        public void AddFactory(ShapeData product, TimeSpan productionRate)
        {
            if (OwnedFactories == null) OwnedFactories = new List<ShapeFactory>();
            OwnedFactories.Add(new ShapeFactory
            {
                ShapeProduct = product,
                ProductionRate = productionRate
            });
        }

        public Boolean TryGetFactory(Predicate<ShapeFactory> predicate, out ShapeFactory factory)
        {
            ShapeFactory[] factories = OwnedFactories.Where(s => predicate?.Invoke(s) ?? false).ToArray();
            factory = factories.FirstOrDefault();
            return factories.Any();
        }

        public Boolean TryGetAllFactories(Predicate<ShapeFactory> predicate, out ShapeFactory[] factories)
        {
            factories = OwnedFactories?.Where(s => predicate?.Invoke(s) ?? false).ToArray();
            return factories?.Any() ?? false;
        }
    }
}
