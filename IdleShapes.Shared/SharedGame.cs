﻿#region Using Statements

using IdleShapes.Shared.Data;
using IdleShapes.Shared.Data.Player;
using IdleShapes.Shared.PlatformInput;
using IdleShapes.Shared.Player;
using IdleShapes.Shared.Screens;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Linq;

#endregion

namespace IdleShapes.Shared
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class SharedGame : Core
    {
        private Scene.SceneResolutionPolicy policy;

        private Entity _Example;

        protected SharedGame(Int32 width = 360, Int32 height = 640, Boolean isFullScreen = false, Single widthScale = 1f, Single heightScale = 1f)
            : base((Int32)(width * widthScale), (Int32)(height * heightScale), isFullScreen)
        {
            policy = Scene.SceneResolutionPolicy.FixedWidth;
            Scene.setDefaultDesignResolution(360, 640, policy);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            UniversalInput.init();
            GameWorld.Init();

            string myName = "J";

            PlayerID[] playerIDs = GameWorld.GetPlayers().Where(s => s.Name.Equals(myName)).ToArray();

            PlayerID playerID;
            if (!playerIDs.Any())
            {
                playerID = GameWorld.CreatePlayer(myName);

                GameWorld.AddFactory(playerID,
                    new ShapeData { Style = ShapeData.ShapeStyle.CIRCLE, Rarity = ShapeData.ShapeRarity.WHITE },
                    TimeSpan.FromSeconds(1));

                GameWorld.AddFactory(playerID,
                    new ShapeData { Style = ShapeData.ShapeStyle.CIRCLE, Rarity = ShapeData.ShapeRarity.BLUE },
                    TimeSpan.FromSeconds(12));

                GameWorld.AddFactory(playerID,
                    new ShapeData { Style = ShapeData.ShapeStyle.CIRCLE, Rarity = ShapeData.ShapeRarity.PURPLE },
                    TimeSpan.FromSeconds(60));

                GameWorld.AddFactory(playerID,
                    new ShapeData { Style = ShapeData.ShapeStyle.TRIANGLE, Rarity = ShapeData.ShapeRarity.GREEN },
                    TimeSpan.FromMinutes(13));
            }
            else
            {
                playerID = playerIDs.First();
            }

            GameWorld.Save(false);

            GameWorld.Load(playerID);

            ActivePlayer.ID = playerID;

            Core.scene = MainScene.GetOrCreate();
        }

        protected override void Update(GameTime gameTime)
        {
            GameWorld.Update(gameTime.ElapsedGameTime);
            base.Update(gameTime);

            UniversalInput.update(gameTime.ElapsedGameTime);
        }

        #region Overrides of Game

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            GameWorld.Save(false);
            base.OnDeactivated(sender, args);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            GameWorld.Save(false);
            base.OnExiting(sender, args);
        }

        protected override void Dispose(bool disposing)
        {
            GameWorld.Save(false);
            base.Dispose(disposing);
        }

        #endregion
    }

}
