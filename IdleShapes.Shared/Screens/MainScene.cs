﻿using IdleShapes.Shared.Data;
using IdleShapes.Shared.Player;
using IdleShapes.Shared.UI;
using IdleShapes.Shared.UI.Animation;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;

namespace IdleShapes.Shared.Screens
{
    public class MainScene : Scene
    {
        private static Lazy<MainScene> _Instance => new Lazy<MainScene>(() => new MainScene());

        private static MainScene Instance => _Instance.Value;

        private Entity _Root;

        private UIElement _PlayerInformation;
        private readonly Dictionary<ShapeData.ShapeStyle, UIElement> _Factories = new Dictionary<ShapeData.ShapeStyle, UIElement>();

        private readonly List<UIElement> _AllElements = new List<UIElement>();

        public static MainScene GetOrCreate()
        {
            return Instance.Init();
        }

        private MainScene() : base()
        {
            this.addRenderer(new DefaultRenderer());
            this.clearColor = Color.CornflowerBlue;
        }

        private void SetEnabledUIElement(UIElement uiElement, Boolean enabled)
        {
            uiElement.enabled = enabled;
            uiElement.TopLeft.enabled = enabled;
            uiElement.TopRight.enabled = enabled;
            uiElement.BottomLeft.enabled = enabled;
            uiElement.BottomRight.enabled = enabled;
            uiElement.Center.enabled = enabled;

            foreach (UIBody uiElementBody in uiElement.Bodies)
            {
                bool e = enabled && uiElement.Open;
                uiElementBody.enabled = e;

                uiElementBody.TopLeft.enabled = e;
                uiElementBody.TopRight.enabled = e;
                uiElementBody.BottomLeft.enabled = e;
                uiElementBody.BottomRight.enabled = e;
                uiElementBody.Center.enabled = e;
            }
        }

        private MainScene Init()
        {
            _Root = this.createEntity("Root");

            _PlayerInformation = new UIElement("PlayerTitle")
            {
                Title = $"{ActivePlayer.ID.Name} - Information",
                parent = _Root.transform,
            };
            _AllElements.Add(_PlayerInformation);

            List<ShapeFactory> list = GameWorld.GetInventory(ActivePlayer.ID).OwnedFactories;
            foreach (ShapeFactory ownedFactory in list)
            {
                if (!_Factories.ContainsKey(ownedFactory.ShapeProduct.Style))
                {
                    _Factories.Add(ownedFactory.ShapeProduct.Style, new UIElement($"{ActivePlayer.ID.Name}-Factory-{ownedFactory.ShapeProduct.Style}"));

                    _Factories[ownedFactory.ShapeProduct.Style].SetAnimation(new UIElementExpandAnimation());
                    _Factories[ownedFactory.ShapeProduct.Style].Title = $"{ownedFactory.ShapeProduct.Style}";


                    _AllElements.Add(_Factories[ownedFactory.ShapeProduct.Style]);
                }

                UIElement e = _Factories[ownedFactory.ShapeProduct.Style];
                UIBody b = new UIBody($"{e.name}-{ownedFactory.ShapeProduct.Style}-{ownedFactory.ShapeProduct.Rarity}")
                {
                    Text = $"{ownedFactory.ShapeProduct.Rarity}: {ownedFactory.ProductsCreated}"
                };

                b.SetAnimation(new UIBodySlideAnimation(e.Bodies.Length % 2 == 0 ? -1 : 1));
                b.SetFactory(ownedFactory);

                e.AddBody(b);
                e.InputEnabled = true;
            }

            for (int i = _AllElements.Count - 1; i > 0; i--) _AllElements[i].parent = _AllElements[i - 1].BottomLeft.transform;

            foreach (UIElement uiElement in _AllElements)
            {
                this.addEntity(uiElement);
                SetEnabledUIElement(uiElement, true);
            }

            return this;
        }

        #region Implementation of IGameWorldListener

        public void OnFactoryProductProduced(ShapeFactory factory, long productsCreated)
        {

        }

        #endregion
    }
}
