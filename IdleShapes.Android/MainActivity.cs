using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Views;

namespace IdleShapes.Android
{
    [Activity(Label = "IdleShapes.Android"
        , MainLauncher = true
        , Icon = "@drawable/icon"
        , Theme = "@style/Theme.Splash"
        , AlwaysRetainTaskState = true
        , LaunchMode = LaunchMode.SingleInstance
        , ScreenOrientation = ScreenOrientation.FullUser
        , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize | ConfigChanges.ScreenLayout)]
    public class MainActivity : Microsoft.Xna.Framework.AndroidGameActivity
    {
        //FIXED Issue with Wiley Fox Swift 2 Plus having random resolution fixed using: https://github.com/MonoGame/MonoGame/issues/6660

        private readonly bool _setImmersive = true;
        private AndroidGame _game;
        private View _view;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            GetDisplaySize(out int width, out int height);
            _game = new AndroidGame(width, height, false);

            _view = (View)_game.Services.GetService(typeof(View));
            SetContentView(_view);

            _game.Run();
        }

        private void GetDisplaySize(out int width, out int height)
        {
            Point realSize = new Point();
            WindowManager.DefaultDisplay.GetRealSize(realSize);

            System.Console.WriteLine("Default Real Size: " + realSize);

            Rect rectSize = new Rect();
            WindowManager.DefaultDisplay.GetRectSize(rectSize);

            System.Console.WriteLine("Default Rect Size: " + rectSize);

            if (_setImmersive) { width = realSize.X; height = realSize.Y; }
            else { width = rectSize.Width(); height = rectSize.Height(); }
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            GetDisplaySize(out int width, out int height);
            _game.ConfigurationChanged(width, height);

            base.OnConfigurationChanged(newConfig);
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);

            if (_setImmersive)
            {
                if (hasFocus)
                    SetImmersive();
            }
        }

        private void SetImmersive()
        {
            if (_view != null /*&& Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat*/)
            {
                _view.SystemUiVisibility = (StatusBarVisibility)
                (SystemUiFlags.LayoutStable |
                SystemUiFlags.LayoutHideNavigation |
                SystemUiFlags.LayoutFullscreen |
                SystemUiFlags.HideNavigation |
                SystemUiFlags.Fullscreen |
                SystemUiFlags.ImmersiveSticky);
            }
        }
    }
}

