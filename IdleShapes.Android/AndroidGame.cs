using IdleShapes.Shared;
using Microsoft.Xna.Framework;
using Nez;
using System;

namespace IdleShapes.Android
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class AndroidGame : SharedGame
    {
        public AndroidGame(Int32 width, Int32 height, Boolean isFullScreen) : base(width, height, isFullScreen)
        {
            Content.RootDirectory = "Content";

            Screen.isFullscreen = true;
            Screen.preferredBackBufferWidth = width;
            Screen.preferredBackBufferHeight = height;

            Screen.supportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight | DisplayOrientation.Portrait | DisplayOrientation.PortraitDown;
            Screen.applyChanges();
        }

        public void ConfigurationChanged(int width, int height)
        {
            Screen.preferredBackBufferWidth = width;
            Screen.preferredBackBufferHeight = height;
            Screen.applyChanges();
        }
    }
}
