﻿using System;

namespace IdleShapes.Desktop
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (DesktopGame game = new DesktopGame())
                game.Run();
        }
    }
}
