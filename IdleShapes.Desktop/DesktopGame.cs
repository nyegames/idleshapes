﻿using IdleShapes.Shared;

namespace IdleShapes.Desktop
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class DesktopGame : SharedGame
    {
        public DesktopGame() : base(isFullScreen: false)
        {
            Window.AllowUserResizing = true;
            Window.IsBorderless = false;
        }
    }
}